import Link from 'next/link';
import styles from './BackToHome.module.css';

const BackToHome = ({ posts }: any) => {
  return (
    <Link href="/">
      <a className={styles.card}>
        <h2>&larr; Home</h2>
        <p>Вернуться домой</p>
      </a>
    </Link>
  );
};

export default BackToHome;
