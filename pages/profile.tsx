import { getSession, GetSessionOptions } from 'next-auth/client';
import styles from '../styles/Profile.module.css';
import BackToHome from '../components/BackToHome/BackToHome';

export const getServerSideProps = async (
  context: GetSessionOptions | undefined
) => {
  const session = await getSession(context);

  if (!session) {
    return {
      redirect: {
        destination: '/api/auth/signin',
        permanent: false
      }
    };
  }

  return {
    props: { user: session.user }
  };
};

const Profile = ({ user }: any) => {
  return (
    <div>
      <BackToHome />
      <h1>Your Profile</h1>

      <div
        style={{ backgroundImage: `url(${user.image})` }}
        className={styles.avatar}
      />
      <p>{user.name}</p>
      <p>{user.email}</p>
    </div>
  );
};

export default Profile;
