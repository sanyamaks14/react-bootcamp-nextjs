import { getSession, GetSessionOptions } from 'next-auth/client';
import styles from '../styles/Posts.module.css';
import BackToHome from '../components/BackToHome/BackToHome';

export const getServerSideProps = async (
  context: GetSessionOptions | undefined
) => {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts');
  const data = await res.json();

  return {
    props: { posts: data }
  };
};

const Posts = ({ posts }: any) => {
  return (
    <div>
      <BackToHome />
      <ul className={styles.posts}>
        {posts.map((post: any) => {
          return (
            <div className={styles.post} key={post.id}>
              <h3>{post.title}</h3>
              <p className={styles.text}>{post.body}</p>
            </div>
          );
        })}
      </ul>
    </div>
  );
};

export default Posts;
