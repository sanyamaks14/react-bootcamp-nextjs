import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import { signIn, signOut, useSession } from 'next-auth/client';
import styles from '../styles/Home.module.css';

export default function Home() {
  const [session, loading] = useSession();

  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <header className={styles['header']}>
        <div
          className={`${styles['user-status']} ${
            !session && loading ? styles.loading : styles.loaded
          }`}>
          {!session && (
            <>
              <p className={styles['text']}>You are not signed in</p>
              <a
                href={`/api/auth/signin`}
                className={styles.button}
                onClick={(e) => {
                  e.preventDefault();
                  signIn();
                }}>
                Sign in
              </a>
            </>
          )}

          {session && (
            <>
              <div className={styles['login-successful']}>
                <p className={styles['text']}>Signed in as</p>
                <div className={styles.user}>
                  {session?.user?.image && (
                    <div
                      style={{ backgroundImage: `url(${session.user.image})` }}
                      className={styles.avatar}
                    />
                  )}
                  <p className={styles['text-bold']}>
                    {session?.user?.email || session?.user?.name}
                  </p>
                </div>
              </div>
              <a
                href={`/api/auth/signout`}
                className={styles.button}
                onClick={(e) => {
                  e.preventDefault();
                  signOut();
                }}>
                Sign out
              </a>
            </>
          )}
        </div>
      </header>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="https://nextjs.org">Next.js!</a>
        </h1>

        <p className={styles.description}>
          Get started by editing{' '}
          <code className={styles.code}>pages/index.js</code>
        </p>

        <div className={styles.grid}>
          <Link href="/posts">
            <a className={styles.card}>
              <h2>Posts &rarr;</h2>
              <p>Почитать поcты</p>
            </a>
          </Link>

          <Link href="/profile">
            <a className={styles.card}>
              <h2>Profile &rarr;</h2>
              <p>
                Эту страницу может просмотреть только авторизованный
                пользователь
              </p>
            </a>
          </Link>

          <Link href="/games">
            <a className={styles.card}>
              <h2>Games &rarr;</h2>
              <p>Вы может просмотреть наши игры</p>
            </a>
          </Link>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer">
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  );
}
